# qm-wdio-appium

UI automation for Android and iOS for Quick Mobile Apps

## Based on
This project is currently based on:
- **WebdriverIO:** `6.##.#`
- **Appium:** `1.15.#`

## Installing Appium on a local machine
See [Installing Appium on a local machine](./docs/APPIUM.md)

## Quick start
Follow the steps and start automating.

1. Clone the git repo — git clone https://aeravathu@bitbucket.org/aeravathu/qm-wdio-appium.git
2. Switch to the repo root folder
3. yarn install from the root of the repo. This will download all the dependencies for the project( make sure that you have yarn installed on your mac machine - brew install yarn)
3. Download the latest .apk and .ipa files and keep it inside the build folder. Name should be qmauto.apk and qmauto.ipa

## How to run tests

NOTE: WDIO will create appium server during the execution and hence we do not require to start the appium server prior to run the tests.

Tests can either run as a suite or as a specific spec file.

1. Run a single spec file - using `--spec` flag
    - Android
        - `yarn local:android --spec /relative path to the spe file`
        - Example: yarn local:android --spec tests/specs/smoke/demoSmokeTest1.spec.js
    - iOS
        - `yarn local:ios --spec /relative path to the spe file`
        - Example: yarn local:ios --spec tests/specs/smoke/demoSmokeTest1.spec.js

2. Run a suite - using `--suite` flag
    - Android
        - SMOKE - `yarn local:smoke:android`
        - REGRESSION - `yarn local:regression:android`
    - iOS
        - SMOKE - `yarn local:smoke:ios`
        - REGRESSION - `yarn local:regression:ios`

## Locator strategy for native apps
The locator strategy for this boilerplate is to use `accessibilityID`'s, see also the [WebdriverIO docs](http://webdriver.io/guide/usage/selectors.html#Accessibility-ID) or this newsletter on [AppiumPro](https://appiumpro.com/editions/20).
`accessibilityID`'s make it easy to script once and run on iOS and Android because most of the apps already have some `accessibilityID`'s.

If `accessibilityID`'s can't be used and for example only XPATH is only available then the following setup could be used to make cross-platform selectors

```js
const SELECTORS = {
    WEB_VIEW_SCREEN: browser.isAndroid
        ? '*//android.webkit.WebView'
        : '*//XCUIElementTypeWebView',
};
```

## Tips and Tricks
See [Tips and Tricks](./docs/TIPS_TRICKS.md)

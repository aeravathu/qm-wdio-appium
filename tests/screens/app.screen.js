import { DEFAULT_TIMEOUT } from '../helpers/constants';

export default class AppScreen {
    constructor (selector) {
        this.selector = selector;
    }

    /**
     * Wait for the screen to be visible
     *
     * @param {boolean} isShown
     * @return {boolean}
     */
    waitForIsShown (isShown = true) {
        return $(this.selector).waitForDisplayed({
            timeout: DEFAULT_TIMEOUT,
            reverse: !isShown,
            timeoutMsg: 'Screen was never displayed',
        });
    }
}

import AppScreen from './app.screen';
import NavBar from './components/containerNavBar';
const SELECTORS = {
    EVENT_SEARCH_FIELD: driver.isAndroid
        ? '//*[@text="Enter your event name or ID"]'
        : '~Enter your event name or ID',
};

class ContainerScreen extends AppScreen {
    constructor () {
        super(SELECTORS.EVENT_SEARCH_FIELD);
    }

    get searchField () {
        return $(SELECTORS.EVENT_SEARCH_FIELD);
    }

    get bottomNavBar () {
        return NavBar;
    }
}

export default new ContainerScreen();

/* global driver $ */

const SELECTORS = {
    CARD_TEXT: driver.isAndroid ? '~menu-list-item-text' : '~menu-list-item',
};

class SlideOutMenu {
    get cardText () {
        return $(SELECTORS.CARD_TEXT);
    }
}

export default new SlideOutMenu();

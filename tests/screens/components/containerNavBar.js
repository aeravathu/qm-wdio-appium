/* global driver $ */

const SELECTORS = {
    MY_EVENTS_TAB: driver.isAndroid ? '//*[@text="My Events"]' : '~My Events',
    SEARCH_TAB: driver.isAndroid ? '//*[@text="Search"]' : '~Search',
    NOTIFICATIONS_TAB: driver.isAndroid
        ? '//*[@text="Notifications"]'
        : '~Notifications',
    SETTINGS_TAB: driver.isAndroid ? '//*[@text="Settings"]' : '~Settings',
};

class NavBar {
    get myEventsTab () {
        return $(SELECTORS.MY_EVENTS_TAB);
    }

    get searchTab () {
        return $(SELECTORS.SEARCH_TAB);
    }

    get notificationsTab () {
        return $(SELECTORS.NOTIFICATIONS_TAB);
    }

    get settingsTab () {
        return $(SELECTORS.SETTINGS_TAB);
    }

    navigateToMyEvents () {
        this.myEventsTab.waitForExist();
        this.myEventsTab.click();
    }

    navigateToSearch () {
        this.searchTab.waitForExist();
        this.searchTab.click();
    }

    navigateToNotifications () {
        this.notificationsTab.waitForExist();
        this.notificationsTab.click();
    }

    navigateToSettings () {
        this.settingsTab.waitForExist();
        this.settingsTab.click();
    }
}

export default new NavBar();

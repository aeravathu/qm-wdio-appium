import ContainerScreen from '../../screens/container.screen';
import Data from '../../../data/testData.json';

const quickEvent = Data.smokeTestQuickEvent;

describe('Container screen', () => {
    beforeAll(() => {
        ContainerScreen.waitForIsShown();
    });
    it('has My events tab.', () => {
        expect(ContainerScreen.bottomNavBar.myEventsTab.isDisplayed()).toEqual(true);
    });
    it('has Search tab.', () => {
        expect(ContainerScreen.bottomNavBar.searchTab.isDisplayed()).toEqual(true);
    });
    it('has Notification tab.', () => {
        expect(ContainerScreen.bottomNavBar.notificationsTab.isDisplayed()).toEqual(true);
    });
    it('has Settings tab.', () => {
        expect(ContainerScreen.bottomNavBar.settingsTab.isDisplayed()).toEqual(true);
    });
    // Try to implement this yourself
    xit('should be able sign up successfully', () => {});
});

describe('Event Search with valid quick event', () => {
    it('should return the correct quick event', () => {
        ContainerScreen.searchField.click();
        ContainerScreen.searchField.setValue(quickEvent.name);
        driver.pause(10000);
        // expect('aa').toEqual('Success\nYou are logged in!');
    });
});

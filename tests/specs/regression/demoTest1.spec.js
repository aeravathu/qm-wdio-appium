import ContainerScreen from '../../screens/container.screen';

describe('Container screen', () => {
    beforeAll(() => {
        ContainerScreen.waitForIsShown();
    });
    it('has My events tab.', () => {
        expect(ContainerScreen.bottomNavBar.myEventsTab.isDisplayed()).toEqual(true);
    });
    it('has Search tab.', () => {
        expect(ContainerScreen.bottomNavBar.searchTab.isDisplayed()).toEqual(true);
    });
    it('has Notification tab.', () => {
        expect(ContainerScreen.bottomNavBar.notificationsTab.isDisplayed()).toEqual(true);
    });
    it('has Settings tab.', () => {
        expect(ContainerScreen.bottomNavBar.settingsTab.isDisplayed()).toEqual(true);
    });
    // Try to implement this yourself
    xit('should be able sign up successfully', () => {});
});

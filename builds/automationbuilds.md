# Automation Builds

1. Download .apk and .ipa for your automation event
2. Rename them to qmauto.ipa and qmauto.apk respectively for iOS and Android
3. Copy and paste those builds in the /build folder.